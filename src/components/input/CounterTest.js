import React, { useState } from 'react'

const CounterTest = () => {
  const [counter, setCounter] = useState(0)

  const onPlus = () => {
    setCounter(counter + 1)
  }

  const onMin = () => {
    setCounter(counter - 1)
  }

  return (<div>
    <h1>counter {counter}</h1>
    <div className='d-flex justify-content-center '>
      <button onClick={onPlus}>+</button>
      <button onClick={onMin}>-</button>
    </div>
  </div>)
}

export default CounterTest