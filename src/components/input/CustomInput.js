import React, { useState } from 'react'

const CustomInput = (props) => {
  let isValid = true
  let errorMessage = ''

  const handleInputChange = (e) => {
    const name = e.target.name
    const value = e.target.value

    props.onChange(name, value)
  }

  if (props.validation) {
    isValid = !((props.validation ?? []).length > 0)
    errorMessage = (props.validation ?? [])[0]
  }

  return (<div>
    <input
      type={props.type ?? 'text'}
      className={`${props.className} text-white bg-dark form-control`}
      name={props.name}
      placeholder={props.placeholder}
      value={props.value}
      onChange={handleInputChange}
    />
    {(!isValid) && (<span className='text-danger'>{errorMessage}</span>)}
  </div>)
}

export default CustomInput