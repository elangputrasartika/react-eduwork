import logo from './logo.svg';
import './App.css';
import Login from './pages/auth/Login';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className='App'>
        <Switch>
          <Route
            exact
            path='/'
            component={Root}
          />
          <Route
            exact
            path='/about'
            component={About}
          />
          <Route
            exact
            path='/users'
            component={Users}
          />
          <Route
            exact
            path='/login'
            component={Login}
          />
        </Switch>
      </div>
    </Router>
  );
}

function About () {
  return (<h1>About</h1>)
}

function Users () {
  return (<h1>Users</h1>)
}

function Root () {
  return (
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        <code>Halo react</code>
      </p>
      <a
        className="App-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
      >
        Learn React
      </a>
    </header>
  )
}

export default App;
