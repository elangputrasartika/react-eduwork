import React, {Component} from 'react'
import CounterTest from '../../components/input/CounterTest'
import CustomInput from '../../components/input/CustomInput'
import Validator from 'validatorjs'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      form: {
        email: '',
        password: ''
      },
      validation: {}
    }
  }

  handleInputChange = (name, value) => {
    this.setState(prevState => ({
      form: {
        ...prevState.form,
        [name]: value
      }
    }), () => {
      console.log(name, value, this.state.form)
    })
  }

  handleFormSubmit = (e) => {
    e.preventDefault()
    let validation = new Validator(this.state.form, {
      email: ['required', 'email'],
      password: ['required', 'min:8']
    });
    const isValid = validation.passes()
    const errors = validation.errors.all()
    console.log(errors,isValid)
    this.setState({validation: errors})
  }

  render() {
    return (
      <div className='container'>
        <CounterTest/>
        <form onSubmit={this.handleFormSubmit} className='p-4'>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email address</label>
            <CustomInput
              validation={this.state.validation['email']}
              name='email'
              placeholder="Enter email"
              value={this.state.form.email}
              onChange={this.handleInputChange}
            />
            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <CustomInput
              type="password"
              validation={this.state.validation['password']}
              name='password'
              placeholder="Password"
              value={this.state.form.password}
              onChange={this.handleInputChange}
            />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </div>
    )
  }
}

export default Login